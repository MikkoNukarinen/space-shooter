﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Boundary {

    public float xMin, xMax, zMin, zMax;
}

public class PlayerController : MonoBehaviour {

    private Rigidbody rb;
    private AudioSource audioSource;
    public float speed = 1;
    public float tilt;
    public Boundary boundary;
    public GameObject shot;
    public Transform shotSpawn;
    private float nextFire;
    public float fireRate = 0.5f;

    private void Start () {

        rb = GetComponent<Rigidbody> ();
        audioSource = gameObject.GetComponent<AudioSource> (); 
    }

    private void Update () {
        if (Input.GetKey(KeyCode.Space) && Time.time > nextFire) {
            nextFire = Time.time + fireRate;
            Instantiate (shot, shotSpawn.position, shotSpawn.rotation);
            audioSource.Play ();
            
        }
    }

    private void FixedUpdate () {
  
        float moveHorizontal = Input.GetAxis ("Horizontal");            
        float moveVertical = Input.GetAxis ("Vertical");
        Vector3 movement = new Vector3 (moveHorizontal, 0.0f, moveVertical);
        rb.velocity = movement * speed;
        
        rb.position = new Vector3 
        (   Mathf.Clamp (rb.position.x, boundary.xMin, boundary.xMax), 
            0.0f, 
            Mathf.Clamp (rb.position.z, boundary.zMin, boundary.zMax)
        );

        // This is how to tilt a player character
        rb.rotation = Quaternion.Euler (0.0f, 0.0f, rb.velocity.x * -tilt);
;    }
}
